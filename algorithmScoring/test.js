function test(str) {
  let obj = {};
  let lth = 0;

  for (let i = 0; i < str.length; i++) {
    if (obj[str[i]]) {
      obj[str[i]] = obj[str[i]] + 1;
    } else {
      obj[str[i]] = 1;
      lth++;
    }
  }

  for (let i = 0; i < lth; i++) {
    if (
      obj["("] !== obj[")"] ||
      obj["["] !== obj["]"] ||
      obj["{"] !== obj["}"]
    ) {
      return false;
    } else {
      return true;
    }
  }
}

console.log(test("({[]})"));
// ({[]}) => true
console.log(test("([][]{})"));
// ([][]{})=> true
console.log(test("({)(]){[}"));
// ({)(]){[} => false
console.log(test("[)()]"));
// [)()] => false
